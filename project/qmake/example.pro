TEMPLATE = subdirs

SUBDIRS *= \
    # $${PWD}/../../module/any/project/qmake/example/scl-any.pro \
    $${PWD}/../../module/feature/project/qmake/example/scl-feature.pro \
    $${PWD}/../../module/meta/project/qmake/example/scl-meta.pro \
    # $${PWD}/../../module/proto/project/qmake/example/scl-proto.pro \
    $${PWD}/../../module/utility/project/qmake/example/scl-utility.pro \
