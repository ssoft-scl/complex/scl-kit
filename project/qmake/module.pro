TEMPLATE = subdirs

CONFIG *= ordered

SUBDIRS *= \
    $${PWD}/../../module/utility/project/qmake/scl-utility.pro \
    $${PWD}/../../module/meta/project/qmake/scl-meta.pro \
    $${PWD}/../../module/feature/project/qmake/scl-feature.pro \
    $${PWD}/../../module/event/project/qmake/scl-event.pro \
    #$${PWD}/../../module/any/project/qmake/scl-any.pro \
    #$${PWD}/../../module/proto/project/qmake/scl-proto.pro \
