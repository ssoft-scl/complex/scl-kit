TEMPLATE = subdirs

SUBDIRS *= \
    # $${PWD}/../../module/any/project/qmake/test/scl-any.pro \
    $${PWD}/../../module/feature/project/qmake/test/scl-feature.pro \
    $${PWD}/../../module/meta/project/qmake/test/scl-meta.pro \
    # $${PWD}/../../module/proto/project/qmake/test/scl-proto.pro \
    $${PWD}/../../module/utility/project/qmake/test/scl-utility.pro \
