cmake_minimum_required( VERSION 3.14 )

add_subdirectory( ${COMPLEX_BASE_DIR}/module/utility/project/cmake ${COMPLEX_BUILD_DIR}/module/utility/project/cmake )
add_subdirectory( ${COMPLEX_BASE_DIR}/module/meta/project/cmake ${COMPLEX_BUILD_DIR}/module/meta/project/cmake )
add_subdirectory( ${COMPLEX_BASE_DIR}/module/feature/project/cmake ${COMPLEX_BUILD_DIR}/module/feature/project/cmake )
add_subdirectory( ${COMPLEX_BASE_DIR}/module/event/project/cmake ${COMPLEX_BUILD_DIR}/module/event/project/cmake )
